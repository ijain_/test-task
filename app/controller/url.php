<?php
namespace App\Controller;

use App\Model\ShortUrl;

class Url
{
    /* Get url parts together with short code
     * @param string $url
     * @return string
     */
    public function makeShort($url)
    {
        $parts = json_decode(json_encode($this->divide($url)));
        $shortCode = (new ShortUrl)->getCode($url);
        return $parts->scheme . "://" . $parts->host . "/" . $shortCode;
    }

    /* Check url response code
   * @return boolean
   */
    public function exists($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return (!empty($response) && $response != 404);
    }

    /* Split url in parts
     * @param string $url
     * @return array
     */
    protected function divide($url)
    {
        return parse_url($url);
    }
}
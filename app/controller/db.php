<?php

namespace App\Controller;

class Db
{
    public static function connect()
    {
        return mysqli_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE);
    }
}
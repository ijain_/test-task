<?php
//include "./app/config/autoloader.php";
require_once __DIR__ . '/vendor/autoload.php';

use App\Model\ShortUrl;
use App\Controller\Url;

$urlData   = new ShortUrl();
$shortCode = $urlData->getCode($_POST['url']);

//update, if exists
if ($shortCode) {
    $urlRecord = $urlData->modify($_POST['url']);
} else { //insert, if url response 200
    if ((new Url)->exists($_POST['url'])) {
        $urlRecord = $urlData->add($_POST['url']);
    } else {
        echo "<p style='color: red;font-weight: bold;'>URL does not exists or not responding.</p>";

        return;
    }
}
$shortUrl = (new Url)->makeShort($_POST['url']);
echo '<a href="#" onclick="myFunction(event, \'' . $_POST['url'] . '\');" id="redirect">' . $shortUrl . '</a>';
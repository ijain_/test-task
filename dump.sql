CREATE DATABASE test_task;
USE test_task;
CREATE TABLE `short_url` (
  `id`   INT(10) UNSIGNED                NOT NULL AUTO_INCREMENT,
  `url`  TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` VARCHAR(30)
         COLLATE utf8mb4_unicode_ci      NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
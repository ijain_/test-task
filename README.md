Installation Instructions:
============================
Platform: Debian Ubuntu 14.04.5 LTS, distribution - Linux Mint 17.3 Rosa
Database: MySql Ver 14.14 Distrib 5.5.54, for debian-linux-gnu
Web Server: Apache/2.4.7 (Ubuntu)
PHP version: 7.0

1) Clone Git repository;

2) Run /test_task/dump.sql to create database and a table;

3) Add your database host, user and password by editing /test_task/app/config/short.php file;

4) Check if you have cURL extension enabled.
If not installed and/or enabled, you will get "curl_init() function not exists" error.

cUrl installation:
a) sudo apt-get install php7.0-curl
b) add extension to php.ini "extension=curl.so"
c) sudo service apache2 restart

5) Go to http://[your_host]/test_task/

Use Case:
==========================
1) User enters a valid URL, more than 30 characters long;
2) User clicks on the button "Do!";
3) The short code is generated and stored in database along with the original URL;
4) The short-code link appears in the "Short URL" section;
5) User clicks on the short URL link;
6) User is redirected to the original URL in a new window/tab.

================================================================================

Инструкция по установке:
===============================
Платформа: Debian Ubuntu 14.04.5 LTS, дистрибутив - Linux Mint 17.3 Роза
База данных: MySql Ver 14.14 Распространяется 5.5.54, для debian-linux-gnu
Веб-сервер: Apache / 2.4.7 (Ubuntu)
Версия PHP: 7.0

1) Clone Git репозиторий;

2) Запустите /test_task/dump.sql для создания базы данных и таблицы;

3) Добавьте хост, пользователь и пароль базы данных, отредактировав файл /test_task/app/config/short.php;

4) Проверьте, включено ли расширение cURL.
Если не установлен и / или не включен, вы получите сообщение об ошибке «curl_init () не существует».

Установка cUrl:
A) sudo apt-get install php7.0-curl
Б) добавить расширение к php.ini "extension = curl.so"
C) перезапуск apache2 для sudo

5) Перейдите по адресу http://[your_host]/test_task/

Вариант использования:
=============================
1) Пользователь вводит действительный URL, длиной более 30 символов;
2) Пользователь нажимает на кнопку «Go!»;
3) Короткий код генерируется и сохраняется в базе данных вместе с исходным URL;
4) Ссылка с коротким кодом появляется в разделе «Короткий URL»;
5) Пользователь нажимает на короткую ссылку URL;
6) Пользователь перенаправляется на исходный URL-адрес в новом окне / вкладке.